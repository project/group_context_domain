<?php

/**
 * @file
 * Hook implementations for the group_context_domain module.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\DomainInterface;
use Drupal\group_context_domain\Plugin\Validation\Constraint\DomainGroupUnique;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Implements hook_entity_type_alter().
 */
function group_context_domain_entity_type_alter(array &$entity_types): void {
  assert($entity_types['domain'] instanceof EntityTypeInterface);
  $entity_types['domain']->addConstraint('DomainGroupUnique');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function group_context_domain_form_domain_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  $cacheable_metadata = (new CacheableMetadata())->addCacheContexts(['user.permissions']);
  $cacheable_metadata->applyTo($form);

  if (!\Drupal::currentUser()->hasPermission('set domain group')) {
    return;
  }

  $group_storage = \Drupal::entityTypeManager()->getStorage('group');
  $group_ids_with_access = $group_storage->getQuery()
    ->addMetaData('op', 'update')
    ->accessCheck()
    ->execute();

  $options = [];
  foreach ($group_storage->loadMultiple($group_ids_with_access) as $group) {
    $options[$group->uuid()] = $group->label();
  }

  $form_object = $form_state->getFormObject();
  assert($form_object instanceof EntityForm);

  $domain = $form_object->getEntity();
  assert($domain instanceof DomainInterface);

  $form['group_uuid'] = [
    '#type' => 'select',
    '#title' => t('Group'),
    '#description' => t('Select the group that represents this domain. This will be returned by the "Group from Domain" context.'),
    '#options' => $options,
    '#default_value' => $domain->getThirdPartySetting('group_context_domain', 'group_uuid'),
    '#empty_value' => '',
  ];

  $form['#entity_builders'][] = '_group_context_domain_save_group_uuid';
  $form['#validate'][] = '_group_context_domain_validate_group_uuid';
}

/**
 * Entity builder for the domain entity.
 */
function _group_context_domain_save_group_uuid($entity_type, $entity, &$form, FormStateInterface $form_state): void {
  if ($group_uuid = $form_state->getValue('group_uuid')) {
    $entity->setThirdPartySetting('group_context_domain', 'group_uuid', $group_uuid);
  }
  else {
    $entity->unsetThirdPartySetting('group_context_domain', 'group_uuid');
  }
}

/**
 * Form validate handler.
 */
function _group_context_domain_validate_group_uuid(&$form, FormStateInterface $form_state): void {
  $form_object = $form_state->getFormObject();
  assert($form_object instanceof EntityForm);

  $domain = $form_object->getEntity();
  assert($domain instanceof DomainInterface);

  $violations = $domain->getTypedData()->validate();
  foreach ($violations as $violation) {
    assert($violation instanceof ConstraintViolation);
    if ($violation->getConstraint() instanceof DomainGroupUnique) {
      $form_state->setErrorByName('group_uuid', $violation->getMessage());
    }
  }
}
